package com.ndk.head.view;

import android.content.Context;

public class DialogManager {
    private static class DialogHelper {
        private static final DialogManager instance = new DialogManager();
    }

    public static DialogManager getInstance() {
        return DialogHelper.instance;
    }

    public void showDialog(String title, Context context, OnCallback onCallback) {
        ConfirmDialog dialog = new ConfirmDialog(context);
        dialog.setTitle(title);
        dialog.setOnDialogClickListener(new ConfirmDialog.OnDialogClickListener() {
            public void onOKClick() {
                onCallback.callback();
                dialog.dismiss();
            }

            public void onCancelClick() {
                dialog.dismiss();
            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    /**
     * dialog确定按钮回调
     */
    public interface OnCallback {
        void callback();
    }
}
