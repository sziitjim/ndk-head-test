package com.ndk.head.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;


import com.ndk.head.R;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

public class ConfirmDialog extends Dialog {
    private Context context;
    private TextView titleTv;
    private TextView tv_content;
    private TextView okBtn, cancelBtn;
    private OnDialogClickListener dialogClickListener;
    private boolean isDismissMissOnOkClick;

    public ConfirmDialog(@NonNull Context context) {
        super(context);
        this.context = context;
        this.isDismissMissOnOkClick = true;
        initalize();
    }

    //初始化View
    private void initalize() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_sdk_confirm, null);
        setContentView(view);
        initWindow();

        titleTv = findViewById(R.id.title_name);
        okBtn = findViewById(R.id.btn_ok);
        cancelBtn = findViewById(R.id.btn_cancel);
        tv_content = findViewById(R.id.tv_content);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDismissMissOnOkClick) {
                    dismiss();
                }
                if (dialogClickListener != null) {
                    dialogClickListener.onOKClick();
                }
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (dialogClickListener != null) {
                    dialogClickListener.onCancelClick();
                }
            }
        });
    }

    public void setTitle(String s) {
        titleTv.setText(s);
    }

    public void setContent(String content) {
        tv_content.setText(content);
        tv_content.setVisibility(View.VISIBLE);
    }

    public void setContentChar(CharSequence content) {
        tv_content.setMovementMethod(LinkMovementMethod.getInstance());
        tv_content.setText(content);
        tv_content.setVisibility(View.VISIBLE);
    }

    public void setOkBtnColor(@ColorRes int color) {
        okBtn.setTextColor(ContextCompat.getColor(context, color));
    }

    public void setDismissMissOnOkClick(boolean dismissMissOnOkClick) {
        isDismissMissOnOkClick = dismissMissOnOkClick;
    }

    public void setOkBtnText(String text) {
        okBtn.setText(text);
    }

    public void setCancelBtnText(String text) {
        cancelBtn.setText(text);
    }

    public TextView getCancelBtn() {
        return cancelBtn;
    }

    public TextView getOkBtn() {
        return okBtn;
    }

    public void hideCancelBtn() {
        cancelBtn.setVisibility(View.GONE);
    }

    /**
     * 添加黑色半透明背景
     */
    private void initWindow() {
        Window dialogWindow = getWindow();
        dialogWindow.setBackgroundDrawable(new ColorDrawable(0));//设置window背景
        //getWindow().setWindowAnimations(R.style.NoAnimationDialog);
        dialogWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);//设置输入法显示模式
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics();//获取屏幕尺寸
        lp.width = (int) (d.widthPixels * 0.85); //宽度为屏幕85%
        lp.gravity = Gravity.CENTER;
        dialogWindow.setAttributes(lp);
    }

    public void setOnDialogClickListener(OnDialogClickListener clickListener) {
        dialogClickListener = clickListener;
    }

    /**
     * 添加按钮点击事件
     */
    public interface OnDialogClickListener {
        void onOKClick();

        void onCancelClick();
    }
}
