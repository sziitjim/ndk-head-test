/*年轻人，只管向前看，不要管自暴自弃者的话*/
package com.ndk.head.uvc.bean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * create by itz on 2024-11-25 15:21
 * desc : {"class_name":"bag_incline","score":0.89,"bbox":[[0,444],[102,742]]}
 **/
public class ImageBean {
    String class_name;
    double score;
    int x1, y1, x2, y2;


    public static List<ImageBean> parseJSON(String jsonString) {
        List<ImageBean> imageBeans = new ArrayList<>();
        try {
            // 创建 JSON 数组
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++) {
                // 创建 JSON 对象
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                ImageBean imageBean = new ImageBean();
                imageBean.class_name = jsonObject.getString("class_name");
                imageBean.score = jsonObject.getDouble("score");

                // 提取边界框数组
                JSONArray bboxArray = jsonObject.getJSONArray("bbox");

                // 解析边界框坐标
                for (int j = 0; j < bboxArray.length(); j++) {
                    JSONArray point = bboxArray.getJSONArray(j);
                    int x = point.getInt(0);
                    int y = point.getInt(1);
                    if (j == 0) {
                        imageBean.x1 = x;
                        imageBean.y1 = y;
                    } else {
                        imageBean.x2 = x;
                        imageBean.y2 = y;
                    }
                    System.out.println("Point " + (j + 1) + ": x = " + x + ", y = " + y);
                }
                imageBeans.add(imageBean);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return imageBeans;
    }

    public String getClassName() {
        return class_name;
    }

    public void setClassName(String class_name) {
        this.class_name = class_name;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }
}
