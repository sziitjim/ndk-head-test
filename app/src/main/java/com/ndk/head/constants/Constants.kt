package com.ndk.head.constants

class Constants {
    companion object {
        /**
         * 存储权限拒绝标志
         */
        const val PERMISSION_STORAGE_DENIED = "PERMISSION_STORAGE_DENIED"

        /**
         * 位置权限拒绝标志
         */
        const val PERMISSION_LOCATION_DENIED = "PERMISSION_LOCATION_DENIED"

        /**
         * 新增的蓝牙权限拒绝标志
         */
        const val PERMISSION_BLUETOOTH_DENIED = "PERMISSION_BLUETOOTH_DENIED"
    }
}