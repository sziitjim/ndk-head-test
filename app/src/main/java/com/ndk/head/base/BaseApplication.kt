package com.ndk.head.base

import android.app.Application
import android.content.Context

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        context = this
    }

    companion object {
        var context: Context? = null

        fun getAppContext(): Context {
            return context!!.applicationContext
        }
    }
}