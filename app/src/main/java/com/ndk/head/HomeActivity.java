package com.ndk.head;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.ndk.head.usb.UsbCameraActivity;
import com.ndk.head.uvc.UvcCameraActivity;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    public void onCheckHead(View view) {
        startActivity(new Intent(this, MainActivity.class));
    }

    public void onUsbCamera(View view) {
        startActivity(new Intent(this, UsbCameraActivity.class));
    }

    public void onUsbCamera2(View view) {
        startActivity(new Intent(this, UvcCameraActivity.class));
    }

    public void sendNative(View view) {

    }
}