package com.ndk.head.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.ndk.head.constants.Constants

class PermissionUtil {
    companion object {
        /**
         * Android 6(23)及以上系统，需要动态申请 相机权限集合
         */
        private val PERMISSION_CAMERA_ARRAY = arrayOf(Manifest.permission.CAMERA)

        /**
         * Android 6(23)及以上系统，需要动态申请 麦克风权限集合
         */
        private val PERMISSION_RECORD_AUDIO_ARRAY = arrayOf(Manifest.permission.RECORD_AUDIO)

        /**
         * Android 6(23)及以上系统，需要动态申请 存储权限集合
         */
        private val PERMISSION_STORAGE_ARRAY = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)

        /**
         * Android 11(30)及以上系统，需要动态申请 存储权限
         */
        private val PERMISSION_MANAGE_STORAGE = arrayOf(Manifest.permission.MANAGE_EXTERNAL_STORAGE)

        /**
         * Android 6及以上系统，需要动态申请 位置权限集合
         */
        private val PERMISSION_LOCATION_ARRAY = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)

        /**
         * Android 12及以上系统，需要动态申请 新增的蓝牙权限集合
         */
        private val PERMISSION_BLUETOOTH_ARRAY = arrayOf(Manifest.permission.BLUETOOTH_SCAN, Manifest.permission.BLUETOOTH_ADVERTISE,
                Manifest.permission.BLUETOOTH_CONNECT)

        /**
         * 全部权限集合
         */
        val PERMISSION_ARRAY = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.BLUETOOTH_SCAN, Manifest.permission.BLUETOOTH_ADVERTISE, Manifest.permission.BLUETOOTH_CONNECT)

        const val REQUEST_CODE = 101

        /**
         * 检测存储权限是否授权
         */
        @JvmStatic
        fun checksStoragePermission(context: Context): Boolean {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
                // Android 11(30)及以上系统，无需动态申请该权限
                return true
            }
            return checksPermission(context, PERMISSION_STORAGE_ARRAY)
        }

        /**
         * 检测蓝牙权限是否授权
         */
        @JvmStatic
        fun checksBluetoothPermission(context: Context): Boolean {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.S) {
                // Android 12(31)及以下系统，无需动态申请权限
                return true
            }
            return checksPermission(context, PERMISSION_BLUETOOTH_ARRAY)
        }

        /**
         * 检测位置权限是否授权
         */
        @JvmStatic
        fun checksLocationPermission(context: Context): Boolean {
            return checksPermission(context, PERMISSION_LOCATION_ARRAY)
        }

        /**
         * 检测android 11 存储权限是否授权
         */
        @JvmStatic
        fun checksManageStoragePermission(): Boolean {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.R) {
                // Android 11(30)及以下系统，无需动态申请权限
                return true
            }
            return Environment.isExternalStorageManager()
        }

        /**
         * 检测麦克风权限是否授权
         */
        @JvmStatic
        fun checksRecordAudioPermission(context: Context): Boolean {
            return checksPermission(context, PERMISSION_RECORD_AUDIO_ARRAY)
        }

        /**
         * 检测相机权限是否授权
         */
        @JvmStatic
        fun checksCameraPermission(context: Context): Boolean {
            return checksPermission(context, PERMISSION_CAMERA_ARRAY)
        }

        private fun checksPermission(context: Context, array: Array<String>): Boolean {
            var granted = true
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M) {
                // Android 6(23)及以下系统，无需动态申请权限
                return granted
            }
            for (permission in array) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    granted = false
                }
            }
            return granted
        }

        /**
         * 申请存储权限
         */
        @JvmStatic
        fun requestStoragePermission(context: Activity) {
            requestPermissions(context, PERMISSION_STORAGE_ARRAY)
        }

        /**
         * 申请android 11 存储权限
         */
        @JvmStatic
        fun requestManageStoragePermission(context: Activity) {
            val intent = Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION)
            intent?.data = Uri.parse("package:" + context.packageName)
            context.startActivity(intent)
        }

        /**
         * 申请蓝牙权限
         */
        @JvmStatic
        fun requestBluetoothPermission(context: Activity) {
            requestPermissions(context, PERMISSION_BLUETOOTH_ARRAY)
        }

        /**
         * 申请麦克风权限
         */
        @JvmStatic
        fun requestRecordAudioPermission(context: Activity) {
            requestPermissions(context, PERMISSION_RECORD_AUDIO_ARRAY)
        }

        /**
         * 申请相机权限
         */
        @JvmStatic
        fun requestCameraPermission(context: Activity) {
            requestPermissions(context, PERMISSION_CAMERA_ARRAY)
        }

        private fun requestPermissions(context: Activity, array: Array<String>) {
            ActivityCompat.requestPermissions(context, array, REQUEST_CODE)
        }

        @JvmStatic
        fun containsBluetooth(permission: String): Boolean {
            return listOf(*PERMISSION_BLUETOOTH_ARRAY).contains(permission)
        }

        /**
         * 拒绝存储权限
         * @return true已拒绝 false 未拒绝
         */
        @JvmStatic
        fun deniedStoragePermission(): Boolean {
            return SharedPre.getInstance().getBoolean(Constants.PERMISSION_STORAGE_DENIED)
        }

        @JvmStatic
        fun deniedBluetoothPermission(): Boolean {
            return SharedPre.getInstance().getBoolean(Constants.PERMISSION_BLUETOOTH_DENIED)
        }
    }
}