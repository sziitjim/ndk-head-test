package com.ndk.head.util

import android.util.Log

class LogUtil {
    companion object {
        @JvmStatic
        fun i(TAG: String, msg: String) {
            Log.i(TAG, msg)
        }
    }
}