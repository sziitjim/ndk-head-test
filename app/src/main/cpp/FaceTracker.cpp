#include "FaceTracker.h"

// 宏定义
#define ANATIVEWINDOW_RELEASE(window)   \
if (window) { \
            ANativeWindow_release(window); \
            window = 0; \
      }

FaceTracker::FaceTracker(const char *model) {
    // 初始化互斥锁
    pthread_mutex_init(&mutex, 0);
    // 创建检测器适配器
    Ptr<CascadeDetectorAdapter> mainDetector = makePtr<CascadeDetectorAdapter>(
            makePtr<CascadeClassifier>(model));
    Ptr<CascadeDetectorAdapter> trackingDetector = makePtr<CascadeDetectorAdapter>(
            makePtr<CascadeClassifier>(model));
    //跟踪器
    DetectionBasedTracker::Parameters DetectorParams;
    tracker = makePtr<DetectionBasedTracker>(DetectionBasedTracker(mainDetector, trackingDetector,
                                                                   DetectorParams));
}

FaceTracker::~FaceTracker() {
    pthread_mutex_destroy(&mutex);
    ANATIVEWINDOW_RELEASE(window);
}

void FaceTracker::setANativeWindow(ANativeWindow *window) {
    pthread_mutex_lock(&mutex);
    // 替换问题
    ANATIVEWINDOW_RELEASE(this->window);
    this->window = window;
    pthread_mutex_unlock(&mutex);
}

/**
 * 绘制图片到屏幕
 * @param img
 */
void FaceTracker::draw(Mat img) {
    pthread_mutex_lock(&mutex);
    do {
        if (!window) {
            break;
        }
        // 设置window格式
        ANativeWindow_setBuffersGeometry(window, img.cols, img.rows,
                                         WINDOW_FORMAT_RGBA_8888);
        // 把需要显示的数据设置给buffer
        ANativeWindow_Buffer buffer;
        if (ANativeWindow_lock(window, &buffer, 0)) {
            ANATIVEWINDOW_RELEASE(window);
            break;
        }
        // 把视频数据刷新到buffer中
        uint8_t *dstData = static_cast<uint8_t *>(buffer.bits);
        int dstlineSize = buffer.stride * 4;
        // 视频图形rgba数据
        uint8_t *srcData = img.data;
        int srclineSize = img.cols * 4;
        // 一行一行的拷贝
        for (int i = 0; i < buffer.height; ++i) {
            memcpy(dstData + i * dstlineSize, srcData + i * srclineSize, srclineSize);
        }
        // 提交渲染
        ANativeWindow_unlockAndPost(window);
    } while (0);
    pthread_mutex_unlock(&mutex);
}
