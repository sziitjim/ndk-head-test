cmake_minimum_required(VERSION 3.10.2)

# TODO 第一步 导入头文件
include_directories(include)

add_library(
        native-lib
        SHARED
        native-lib.cpp
        FaceTracker.cpp
        ImageCheck.cpp)

# TODO 第二步 导入库文件
# ${CMAKE_CXX_FLAGS} 原先的CMake环境 +
# -L${CMAKE_SOURCE_DIR}/../jniLibs/${CMAKE_ANDROID_ARCH_ABI} 加上新的fmod库的路径
# CMAKE_SOURCE_DIR == CMakeLists.txt所在的路径
# /../ 回退到cpp目录
# jniLibs/ 进入jniLibs目录
# ${CMAKE_ANDROID_ARCH_ABI} 使用当前手机CPU系统结构对应的库 如：arm64-v8a
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -L${CMAKE_SOURCE_DIR}/../jniLibs/${CMAKE_ANDROID_ARCH_ABI}")

target_link_libraries(
        native-lib
        opencv_java4
        android
        log)