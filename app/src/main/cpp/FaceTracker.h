#ifndef NDKHEADTEST_FACETRACKER_H
#define NDKHEADTEST_FACETRACKER_H

#include <android/native_window_jni.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/types_c.h>
#include <pthread.h>

/**
 * OpenCV 检测器适配器
 */
using namespace cv;

class CascadeDetectorAdapter : public DetectionBasedTracker::IDetector {
public:
    CascadeDetectorAdapter(cv::Ptr<cv::CascadeClassifier> detector) :
            IDetector(),
            Detector(detector) {
        CV_Assert(detector);
    }

    void detect(const cv::Mat &Image, std::vector<cv::Rect> &objects) {
        Detector->detectMultiScale(Image, objects, scaleFactor, minNeighbours, 0, minObjSize,
                                   maxObjSize);
    }

    virtual ~CascadeDetectorAdapter() {
    }

private:
    CascadeDetectorAdapter();

    cv::Ptr<cv::CascadeClassifier> Detector;
};

/**
 * 人脸跟踪
 */
class FaceTracker {

public:
    FaceTracker(const char *string);

    ~FaceTracker();

public:
    Ptr<DetectionBasedTracker> tracker;
    pthread_mutex_t mutex; // 互斥锁
    ANativeWindow *window = 0; // ANativeWindow 用来渲染画面的 == Surface对象

public:
    void setANativeWindow(ANativeWindow *window);
    void draw(Mat mat);
};


#endif //NDKHEADTEST_FACETRACKER_H
